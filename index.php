<?php
require "vendor/autoload.php"; // Inclusion de l'autoloader
$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function ($string) {
        return renderHTMLFromMarkdown($string);
    }));
});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

Flight::route('/', function () {
    $data = [
        'dinos' => getdino(),
    ];
    Flight::render('liste.twig', $data);
});

Flight::route('/dinosaur/@slug', function ($slug) {
    $data = [
        'dino' => getonedino($slug),
    ];
    Flight::render('dino.twig', $data);
});

Flight::start();