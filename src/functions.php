<?php
function getdino()
{
    $response = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/');
    return json_decode($response->body);

}

function getonedino($slug)
{
    $response = Requests::get("https://allosaurus.delahayeyourself.info/api/dinosaurs/$slug");
    return json_decode($response->body);

}

function readFileContent($filepath)
{
    if (file_exists($filepath)) {
        return file_get_contents($filepath);
    }
    throw new Exception("File doesn't exist!");
}

function getPageContent($page_name)
{
    $filepath = sprintf("pages/%s.md", $page_name);
    return readFileContent($filepath);
}